# SpaceShooter

A simple space shooter game where you can shoot enemies.

# Credits

- [kenney.nl](https://kenney.nl/assets/space-shooter-redux) for assets
- [Ronald Kah](https://ronaldkah.de) for Musics
