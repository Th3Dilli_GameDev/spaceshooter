extends Control

# Level to load when pressing Start
export (PackedScene) var level
export (PackedScene) var choose


func _ready() -> void:
	$SettingsCtrl.connect("backBtnPressed", self , "_on_BackBtn_pressed")
	$ChooseShip.connect("backBtnPressed", self , "_on_BackBtn_pressed")


func _on_QuitBtn_pressed() -> void:
	get_tree().quit(0)


func _on_StartBtn_pressed() -> void:
	get_tree().change_scene_to(level)


func _on_SettingsBtn_pressed() -> void:
	$SettingsCtrl.show()
	$MainCtrl.hide()


func _on_BackBtn_pressed() -> void:
	$ChooseShip.hide()
	$SettingsCtrl.hide()
	$MainCtrl.show()



func _on_ChangeShipBtn_pressed() -> void:
	$MainCtrl.hide()
	$ChooseShip.show()
