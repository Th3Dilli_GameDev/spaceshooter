extends Control

var type :int
var color : int
var colors = ["blue", "green", "orange", "red"]
var settings: Settings
signal backBtnPressed

func _ready() -> void:
	settings = get_node("/root/Settings")
	type = settings.shipType
	color = settings.shipColor
	update()


func update() -> void:
	settings.shipType = type 
	settings.shipColor = color
	$VBoxContainer/TextureRect.texture = load("res://assets/textures/Player/playerShip"+str(type+1)+"_"+colors[color]+".png")



func _on_TypeBtnL_pressed() -> void:
	if (type == 2):
		type = 0
	else:
	 type += 1
	update() 


func _on_TypeBtnR_pressed() -> void:
	if (type == 0):
		type = 2
	else:
	 type -= 1
	update() 


func _on_ColorBtnL_pressed() -> void:
	if (color == 3):
		color = 0
	else:
	 color += 1
	update() 


func _on_ColorBtnR_pressed() -> void:
	if (color == 0):
		color = 3
	else:
	 color -= 1
	update() 


func _on_MenuBtn_pressed() -> void:
	emit_signal("backBtnPressed")
