extends Control

func _ready() -> void:
	$SettingsCtrl.connect("backBtnPressed", self , "_on_BackBtn_pressed")


func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		if !get_tree().paused:
			tooglePause()


func _on_SettingsBtn_pressed() -> void:
	$PauseCtrl.hide()
	$SettingsCtrl.show()


func _on_BackBtn_pressed() -> void:
	$SettingsCtrl.hide()
	$PauseCtrl.show()


func _on_ResumeBtn_pressed() -> void:
	tooglePause()


func tooglePause():
	get_tree().paused = !get_tree().paused
	if $PauseCtrl.visible :
		$PauseCtrl.hide()
	else:
		$PauseCtrl.show()


func _on_MainMenuBtn_pressed() -> void:
	tooglePause()
	get_tree().change_scene("res://src/ui/MainMenu.tscn")

func end() -> void:
	get_tree().paused = !get_tree().paused
	$Endscreen.show()
	$Endscreen.updateScore()

