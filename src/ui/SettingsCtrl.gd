extends Control

signal backBtnPressed
var settings: Settings
var globalsignals: GlobalSignals

func _ready() -> void:
	settings = get_node("/root/Settings")
	globalsignals = get_node("/root/GlobalSignals")
	$SettingsCont/MusicSlider.value = settings.music
	AudioServer.set_bus_volume_db(1, settings.music)
	$SettingsCont/EffectSlider.value = settings.effects
	AudioServer.set_bus_volume_db(2, settings.effects)


func save() -> void:
	settings.music = $SettingsCont/MusicSlider.value
	settings.effects = $SettingsCont/EffectSlider.value


func _on_BackBtn_pressed() -> void:
	emit_signal("backBtnPressed")


func _on_MusicSlider_value_changed(value: float) -> void:
	globalsignals.emit_signal("MusicChanged",value)
	AudioServer.set_bus_volume_db(1, value)
	settings.music = $SettingsCont/MusicSlider.value


func _on_EffectSlider_value_changed(value: float) -> void:
	globalsignals.emit_signal("EffectChanged",value)
	AudioServer.set_bus_volume_db(2, value)
	settings.effects = $SettingsCont/EffectSlider.value
