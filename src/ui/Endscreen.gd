extends Control

var player_stats

func _ready():
	player_stats = get_node("/root/PlayerStats")
	$ChooseShip.connect("backBtnPressed", self, "_on_BackBtn_pressed")


func updateScore() -> void:
	$EndControl/Label_score.text = "Your score: " + String(player_stats.score)

func _on_MainMenuBtn_pressed():
	get_tree().paused = false
	get_tree().change_scene("res://src/ui/MainMenu.tscn")
	

func _on_RestartBtn_pressed():
	get_tree().paused = false
	get_tree().change_scene("res://src/levels/MainLevel.tscn")


func _on_ChangeShip_pressed() -> void:
	$ChooseShip.show()
	$EndControl.hide()

func _on_BackBtn_pressed() -> void:
	$ChooseShip.hide()
	$EndControl.show()
