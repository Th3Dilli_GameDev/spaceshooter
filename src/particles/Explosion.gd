extends Particles2D

func explode() -> void:
	restart()
	$Explosion.restart()
