extends Control

export (Array,PackedScene) var enemyTypes
export (PackedScene) var PlayerLaser
export (PackedScene) var LaserPowerUp_green
export (PackedScene) var LaserPowerUp_purple

var counter = 0
var spawn_speed = 4
var score = 0
var player_stats: PlayerStats
var settings: Settings

var player: Player
var green_powerup_spawned = false
var purple_powerup_spawned = false

func _ready():
	var globalsignals = get_node("/root/GlobalSignals")
	player_stats = get_node("/root/PlayerStats")
	settings = get_node("/root/Settings")
	player = load("res://src/player/Player"+ str(settings.shipType + 1) +".tscn").instance()
	player.position = Vector2(100,400)
	$Laser.add_child(player)
	$Timer.wait_time = spawn_speed
	$HUD/Control/Hitpoints.text = String(player.hitpoints)
	$HUD/Control/Score.text = String(score)
	globalsignals.connect("EnemyDied", self, "onEnemyDied")
	player.connect("hit", self, "_on_Player_hit")
	

func onEnemyDied(points) -> void:
	$Enemie_died.play()
	score += points
	$HUD/Control/Score.text = str(score)
	player_stats.score = score

func _on_Timer_timeout():
	# spawn enemies
	updateStats()
	var i = randi() % enemyTypes.size()
	var spawned_enemy = enemyTypes[i].instance()
	var y = rand_range(55 ,(get_viewport().size.y) - 55)
	spawned_enemy.position = Vector2(get_viewport().size.x ,y) 
	spawned_enemy.add_to_group("enemies")
	$Enemies.add_child(spawned_enemy)
	
	# always spawn a green powerup on score 10 and a purple one on 30
	if score >= 10 and green_powerup_spawned != true:
		spawn_powerup()
		green_powerup_spawned = true
		
	if score >= 30 and purple_powerup_spawned != true:
		spawn_powerup()
		purple_powerup_spawned = true
	
	# trigger spawning of powerups with a 20% probability
	var spawn_laser_powerup = (randi() % 101) <= 20
	
	if spawn_laser_powerup and score >= 10:
		spawn_powerup()


func spawn_powerup():
	var powerup
	
	if score >= 10 and score <= 30:
		print("Spawning green powerup")
		powerup = LaserPowerUp_green.instance()
		
	if score >= 30:
		print("Spawning purple powerup")
		powerup = LaserPowerUp_purple.instance()

	var y_pow = rand_range(55 ,(get_viewport().size.y) - 55)
	powerup.position = Vector2(get_viewport().size.x, y_pow)
	powerup.add_to_group("items")
	$Items.add_child(powerup)


func updateStats() -> void:
	counter = counter + 1
	if counter == 25:
		counter = 0
		if spawn_speed == 1:
			spawn_speed = spawn_speed - 0.5
			$Timer.wait_time = spawn_speed
		else:
			spawn_speed = spawn_speed -1
			$Timer.wait_time = spawn_speed

func _on_Player_hit():
	$HUD/Control/Hitpoints.text = str(player.hitpoints)
	if player.hitpoints == 0:
		$PauseMenu.end()
