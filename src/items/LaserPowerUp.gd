extends Area2D

class_name LaserPowerUp

var speed = 120

func _ready():
	pass 

func _process(delta):
	position.x = position.x - (speed * delta)


func _on_LaserPowerUp_area_entered(area):
	queue_free()
