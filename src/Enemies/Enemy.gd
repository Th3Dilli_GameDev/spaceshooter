extends Area2D
class_name Enemy

var velocity = Vector2()
var hintpoints = 2
var speed = 100
var points = 1
export var fire_rate :float = 1


export (PackedScene) var explosion
export (PackedScene) var collision
export (PackedScene) var EnemyLaser

func _ready() -> void:
	$FireRate.set_wait_time(1 / fire_rate)
	$FireRate.connect("timeout", self, "FireRate_timeout")
	$FireRate.start()

func FireRate_timeout() -> void:
	var enemyLaser = EnemyLaser.instance()
	enemyLaser.global_position = to_global($LaserPostion.position)
	get_parent().add_child(enemyLaser)

# if enemy leaves screeb, it will disappear
func _on_VisibilityNotifier2D_screen_exited():
	queue_free()


func _process(delta: float) -> void:
	position.x = position.x - (speed * delta)



func _on_Enemy_area_entered(area: Area2D) -> void:
	if(area is Player):
		spawnExpl(collision)
	elif (area is Laser):
		spawnExpl(explosion)
	got_hit()


func got_hit() -> void:
	hintpoints = hintpoints - 1
	if hintpoints == 0:
		var globalsignals = get_node("/root/GlobalSignals")
		globalsignals.emit_signal("EnemyDied", points)
		queue_free()
	else:
		$Enemie_hit.play()


func spawnExpl(type) -> void:
	var expl = type.instance()
	# get_parent returns the Enemies node from the main level
	get_parent().add_child(expl)
	expl.global_position = to_global($hitPos.position)
	expl.explode()
