extends Area2D
class_name EnemyLaser

var speed = 1000

func _process(delta: float) -> void:
	position.x = position.x - (speed * delta)


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()


func _on_EnemyLaser_area_entered(area):
	queue_free()
