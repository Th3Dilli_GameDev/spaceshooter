extends Area2D
class_name Laser

var speed = 1000
var y_mov = 0

func init_y_movement(y_mov: int):
	self.y_mov = y_mov

func _process(delta: float) -> void:
	position.x = position.x + (speed * delta)
	position.y = position.y + (speed * delta) * y_mov

func _on_VisibilityNotifier2D_screen_exited() -> void:
	queue_free()


func _on_PlayerLaser_area_entered(area: Area2D) -> void:
	queue_free()
