extends Area2D
class_name Player

export (PackedScene) var PlayerLaser
export (PackedScene) var PlayerLaser_green
export (PackedScene) var PlayerLaser_purple

export var player_speed = 400
export var fire_rate :float = 4
signal hit

enum LASERMODES {
	STRAIGHT,
	DOUBLE,
	V
}

enum LASERCOLORS {
	BLUE,
	GREEN,
	PURPLE
}

var laser_modes = [LASERMODES.STRAIGHT, LASERMODES.DOUBLE, LASERMODES.V]
var current_laser_mode = 0
var current_laser_color = LASERCOLORS.BLUE

var can_shoot = true
var hitpoints = 5


var settings: Settings
var ship_colors = ["blue", "green", "orange", "red"]

func _ready() -> void:
	settings = get_node("/root/Settings")
	$Sprite.texture = load("res://assets/textures/Player/playerShip"+str(settings.shipType+1)+"_"+ship_colors[settings.shipColor]+".png")
	$FireRate.set_wait_time(1 / fire_rate)
	$FireRate.connect("timeout", self, "FireRate_timeout")


func _process(delta: float) -> void:
	if Input.is_action_pressed("move_up"):
		position.y = max(position.y - player_speed * delta , 0)
	if Input.is_action_pressed("move_right"):
		position.x = min(position.x + player_speed * delta , get_viewport().size.x)
	if Input.is_action_pressed("move_left"):
		position.x = max(position.x - player_speed * delta , 0)
	if Input.is_action_pressed("move_down"):
		position.y = min(position.y + player_speed * delta , get_viewport().size.y)
	if Input.is_action_just_pressed("next_laser_mode"):
		current_laser_mode = (current_laser_mode+1) % laser_modes.size()
		print("Laser mode toggled: " + str(current_laser_mode) + " of " + str(laser_modes.size()))
	if Input.is_action_pressed("shoot"):
		shoot()


func shoot() -> void:
	if can_shoot:
		can_shoot = false
		
		$LaserShoot.play()
		
		match laser_modes[current_laser_mode]:
			LASERMODES.STRAIGHT:
				$FireRate.set_wait_time(1 / fire_rate)
				shoot_single($LaserPosition_center.position, 0, 0)
				
			LASERMODES.DOUBLE:
				$FireRate.set_wait_time(1 / fire_rate * 2)
				shoot_double()
				
			LASERMODES.V:
				$FireRate.set_wait_time(1 / fire_rate * 2)
				shoot_v()
				
		$FireRate.start()


func shoot_single(local_pos: Vector2, move_y: int, rotate_texture: int) -> void:
	var laser
	
	match current_laser_color:
		LASERCOLORS.BLUE:
			laser = PlayerLaser.instance()
		LASERCOLORS.GREEN:
			laser = PlayerLaser_green.instance()
		LASERCOLORS.PURPLE:
			laser = PlayerLaser_purple.instance()
	
	laser.global_position = to_global(local_pos)
	laser.add_to_group("laser")
	laser.init_y_movement(move_y)
	laser.rotate(rotate_texture)
	get_parent().add_child(laser)
	
func shoot_double() -> void:
	shoot_single($LaserPosition_topWing.position, 0, 0)
	shoot_single($LaserPosition_bottomWing.position, 0, 0)
	
func shoot_v():
	shoot_single($LaserPosition_topTip.position, -1, 40)
	shoot_single($LaserPosition_bottomTip.position, 1, -40)

func FireRate_timeout() -> void:
	can_shoot = true

func _on_Player_area_entered(area):
	print("player onent: "+area.name + " : " + str(hitpoints))
	if (hitpoints > 0):
		if ("EnemyLaser" in area.name):
			$MainExp.explode()
			$Laser.play()
			hitpoints -= 1
			emit_signal("hit")
		elif ("Enemy" in area.name):
			$CollisionPart.restart()
			$Collision.play()
			hitpoints -= 1
			emit_signal("hit")
		elif ("LaserPowerUp_green" in area.name):
			print("Green powerup consumed")
			current_laser_color = LASERCOLORS.GREEN
			fire_rate = fire_rate * 1.05
		elif (area.name == "LaserPowerUp_purple"):
			print("Purple powerup consumed")
			current_laser_color = LASERCOLORS.PURPLE
			fire_rate = fire_rate * 1.1
	

func _on_Timer_shield_timeout():
	$Shield.hide()

func _on_AnimatedSprite_animation_finished() -> void:
	$collisionAnim.stop()
	$collisionAnim.frame = 0
