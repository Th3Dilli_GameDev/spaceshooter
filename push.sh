butler push --if-changed builds/html th3dilli/SpaceShooter:HTML
butler push --if-changed builds/win th3dilli/SpaceShooter:win
butler push --if-changed --fix-permissions builds/mac th3dilli/SpaceShooter:mac
butler push --if-changed --fix-permissions builds/linux th3dilli/SpaceShooter:linux
